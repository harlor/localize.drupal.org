FROM registry.access.redhat.com/ubi8/ubi-minimal:latest AS base

ENV OPCACHE_REVALIDATE_FREQ 600
ENV PHP_MEMORY_LIMIT 256M
ENV COMPOSER_CACHE_DIR=/dev/null

USER 0
WORKDIR /app

RUN { \
        echo '[mariadb]'; \
        echo 'name = MariaDB'; \
        if [ `arch` ==  "x86_64" ]; then echo 'baseurl = https://archive.mariadb.org/mariadb-10.5/yum/rhel8-amd64'; fi; \
        if [ `arch` == "aarch64" ]; then echo 'baseurl = https://archive.mariadb.org/mariadb-10.5/yum/rhel8-aarch64'; fi; \
        echo 'gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB'; \
        echo 'gpgcheck=1'; \
} > /etc/yum.repos.d/mariadb.repo

RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && \
    rpm -Uvh https://rpms.remirepo.net/enterprise/remi-release-8.rpm && \
    microdnf module enable -y php:remi-8.1 && \
    microdnf install -y MariaDB-client \
    git \
    tzdata \
    httpd \
    patch \
    php \
    php-cli \
    php-gd \
    php-gmp \
    php-intl \
    php-odbc \
    php-pdo \
    php-bcmath \
    php-json \
    php-mbstring \
    php-mysqlnd \
    php-xml \
    php-xmlrpc \
    php-pgsql \
    php-soap \
    php-pecl-apcu \
    php-opcache \
    php-pecl-zip \
    findutils && \
    microdnf update -y tzdata && \
    microdnf reinstall tzdata -y && \
    microdnf clean -y all

# Install composer.
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer && \
    curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig && \
    php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" && \
    php /tmp/composer-setup.php --filename composer --install-dir /usr/local/bin

# Set recommended opcache settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=${OPCACHE_MEMORY_CONSUMPTION}'; \
		echo 'opcache.interned_strings_buffer=16'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
} > /etc/php.d/10-opcache.ini

RUN { \
		echo 'expose_php=Off'; \
		echo 'memory_limit=${PHP_MEMORY_LIMIT}'; \
} > /etc/php.d/20-php-sec-defaults.ini

#Read XFF headers, note this is insecure if you are not sanitizing
#XFF in front of the container
RUN { \
    echo '<IfModule mod_remoteip.c>'; \
    echo '  RemoteIPHeader X-Forwarded-For'; \
    echo '</IfModule>'; \
} > /etc/httpd/conf.d/remoteip.conf

#Correctly set SSL if we are terminated by it
RUN { \
    echo 'SetEnvIf X-Forwarded-Proto "https" HTTPS=on'; \
} > /etc/httpd/conf.d/remote_ssl.conf

#Enable prefork for mod_php
RUN { \
    echo 'LoadModule mpm_prefork_module modules/mod_mpm_prefork.so'; \
} > /etc/httpd/conf.modules.d/00-mpm.conf

RUN sed -i 's/Listen 80/Listen 8080/' /etc/httpd/conf/httpd.conf && \
    chgrp -R 0 /var/log /var/run /run/httpd && \
    chmod -R g=u  /var/log /var/run /run/httpd

RUN { \
    echo '<VirtualHost *:8080>'; \
    echo 'DocumentRoot /app/web'; \
    echo '<Directory /app/web >'; \
    echo '    AllowOverride All'; \
    echo '    Require all granted';\
  echo '</Directory>'; \
  echo '</VirtualHost>';\
} > /etc/httpd/conf.d/vhost.conf

FROM base AS build

# Copy site code
COPY . .

# Install composer dependencies.
RUN composer install --no-ansi --no-dev --no-interaction --no-progress --prefer-dist --optimize-autoloader && \
    mkdir -pv web/sites/default/files && \
    chown -R 1001:0 web/sites/default/files && \
    chmod -R 770 web/sites/default/files && \
    find . -type d -name ".git" | xargs rm -rf


FROM base AS web

ENV PATH "/app/vendor/bin:$PATH"
WORKDIR /app

COPY --from=build /app /app

RUN mkdir -pv git_checkout && chown 1001:0 git_checkout && chmod g+rw git_checkout

USER 1001

EXPOSE 8080

CMD ["httpd", "-D", "FOREGROUND"]
